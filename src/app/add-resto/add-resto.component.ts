import { Component, OnInit } from '@angular/core';
import { FormBuilder, Validators, FormControl, FormGroup, FormGroupName } from '@angular/forms';
import { RestoService } from '../resto.service';
import { HttpClient } from '@angular/common/http';

@Component({
  selector: 'app-add-resto',
  templateUrl: './add-resto.component.html',
  styleUrls: ['./add-resto.component.css']
})
export class AddRestoComponent implements OnInit {

  constructor(
    private resto: RestoService
  ) { }


  addResto = new FormGroup({
    name: new FormControl(''),
    address: new FormControl(''),
    email: new FormControl('')
  });

  alertStatus:boolean = false; 
  collectResto() {
    this.resto.saveResto(this.addResto.value).subscribe((result)=>{
        this.alertStatus = true;
    })
    this.addResto.reset({});
  }

  closeAlert() {
    this.alertStatus = false;
  }

  

  ngOnInit(): void {
  }

}
