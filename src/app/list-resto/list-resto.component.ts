import { Component, OnInit } from '@angular/core';
import { RestoService } from '../resto.service';
import { HttpClient } from '@angular/common/http';

@Component({
  selector: 'app-list-resto',
  templateUrl: './list-resto.component.html',
  styleUrls: ['./list-resto.component.css']
})
export class ListRestoComponent implements OnInit {

  collection: any;
  constructor(
    private resto: RestoService
  ) { }

  displayList() {
    this.resto.getList().subscribe((result) => {
      this.collection = result;
    })
  }

  ngOnInit(): void {
      this.displayList()
  }

  

  deleteResto(resto_id) {
    this.resto.deleteResto(resto_id).subscribe((resultDelete) => {
        this.displayList()
    })
  }

}
