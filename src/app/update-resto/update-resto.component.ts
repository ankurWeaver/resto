import { Component, OnInit } from '@angular/core';
import { FormBuilder, Validators, FormControl, FormGroup, FormGroupName } from '@angular/forms';
import { RestoService } from '../resto.service';
import { HttpClient } from '@angular/common/http';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-update-resto',
  templateUrl: './update-resto.component.html',
  styleUrls: ['./update-resto.component.css']
})
export class UpdateRestoComponent implements OnInit {

  constructor(
    private route: ActivatedRoute,
    private resto: RestoService
  ) { }

  updateResto = new FormGroup({
    name: new FormControl(''),
    address: new FormControl(''),
    email: new FormControl('')
  });

  ngOnInit(): void {
    this.getRestoById(this.route.snapshot.params.id);
  }

  getRestoById(id) {
    this.resto.getListById(id).subscribe((restaurant) => {
      this.updateResto = new FormGroup({
        name: new FormControl(restaurant['name']),
        address: new FormControl(restaurant['address']),
        email: new FormControl(restaurant['email'])
      });
    });
  }

  alertStatus: boolean = false;
  collectResto() {
     this.resto.updateResto(this.updateResto.value,this.route.snapshot.params.id).subscribe((restaurant)=>{
          this.alertStatus = true;
          this.updateResto = new FormGroup({
            name: new FormControl(restaurant['name']),
            address: new FormControl(restaurant['address']),
            email: new FormControl(restaurant['email'])
          });
     })
  }

  closeAlert() {
    this.alertStatus = false;
  }

}
