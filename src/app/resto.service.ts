import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class RestoService {
  data:any;
  apiUrl:any;
  retrievedData:any;
  constructor(
    private http: HttpClient
  ) { }

  getList(){
    this.apiUrl = 'http://localhost:3000/restaurants';
    return this.http.get(this.apiUrl)  
  }

  getListById(id){
    this.apiUrl = 'http://localhost:3000/restaurants/'+id;
    return this.http.get(this.apiUrl)  
  }
  
  saveResto(data) {
      this.apiUrl = 'http://localhost:3000/restaurants';
      return this.http.post(this.apiUrl,data)
  }

  deleteResto(id) {
    this.apiUrl = 'http://localhost:3000/restaurants/'+id;
    return this.http.delete(this.apiUrl)
  }

  updateResto(restoData,id) {
    this.apiUrl = 'http://localhost:3000/restaurants/'+id;
    return this.http.put(this.apiUrl, restoData)
  }

  



}
